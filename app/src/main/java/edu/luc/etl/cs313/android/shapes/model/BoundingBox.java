package edu.luc.etl.cs313.android.shapes.model;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// DONE

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
        return f.getShape().accept(this);
	}

	@Override
	public Location onGroup(final Group g) {

        int minX = g.getShapes().get(0).accept(this).getX();
        int maxX = g.getShapes().get(0).accept(this).getX();
        int minY = g.getShapes().get(0).accept(this).getY();
        int maxY = g.getShapes().get(0).accept(this).getY();

        for (int i = 0; i < g.getShapes().size(); i++)
        {
            final Rectangle r = (Rectangle) g.getShapes().get(i).accept(this).getShape();

            if (g.getShapes().get(i).accept(this).getX() + r.getWidth() <= minX)
                minX = g.getShapes().get(i).accept(this).getX() + r.getWidth();

            if (g.getShapes().get(i).accept(this).getX() + r.getWidth() >= maxX)
                maxX = g.getShapes().get(i).accept(this).getX() + r.getWidth();

            if (g.getShapes().get(i).accept(this).getY() + r.getHeight() <= minY)
                minY = g.getShapes().get(i).accept(this).getY() + r.getHeight();

            if (g.getShapes().get(i).accept(this).getY() + r.getHeight() >= maxY)
                maxY = g.getShapes().get(i).accept(this).getY() + r.getHeight();
        }

        int deltaX = maxX - minX;
        int deltaY = maxY - minY;

        return new Location(minX, minY, new Rectangle(deltaX, deltaY));
    }

	@Override
	public Location onLocation(final Location l) {
        final Location b = l.getShape().accept(this);
        return new Location(l.getX() + b.getX(), l.getY() + b.getY(), b.getShape());

    }

	@Override
	public Location onRectangle(final Rectangle r) {
		return new Location(0,0, r);
	}

	@Override
	public Location onStroke(final Stroke c) {
        //May need fixing on account of using onRectangle (may not be correct)
        return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {
        //May need fixing on account of using onRectangle (may not be correct)
        return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {

        int minX = s.getPoints().get(0).getX();
        int maxX = s.getPoints().get(0).getX();
        int minY = s.getPoints().get(0).getX();
        int maxY = s.getPoints().get(0).getX();

        for (int i = 0; i < s.getPoints().size(); i++)
            {
                if (s.getPoints().get(i).getX() >= maxX)
                    maxX = s.getPoints().get(i).getX();

                if (s.getPoints().get(i).getY() >= maxY)
                    maxY =s.getPoints().get(i).getY();

                if (s.getPoints().get(i).getX() <= minX)
                    minX = s.getPoints().get(i).getX();

                if (s.getPoints().get(i).getY() <= minY)
                    minY = s.getPoints().get(i).getY();
        }

        int deltaX = maxX - minX, deltaY = maxY - minY;

        return new Location(minX ,minY ,new Rectangle (deltaX, deltaY));
	}
}
