package edu.luc.etl.cs313.android.shapes.model;

/**
 * A point, implemented as a location without a shape.
 */
public class Point extends Location {

	// DONE
	// HINT: use a circle with radius 0 as the shape!
    public static final Shape c = new Circle(0);

	public Point(final int x, final int y)
    {
		super(x, y, c);
		assert x >= 0;
		assert y >= 0;
	}
}

